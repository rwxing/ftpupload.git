package com.zhu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.zhu.dao")
public class FtpuploadApplication {

	public static void main(String[] args) {
		SpringApplication.run(FtpuploadApplication.class, args);
	}
}
