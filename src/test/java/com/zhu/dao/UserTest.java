package com.zhu.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.zhu.pojo.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {
	@Autowired
	private UserMapper userMapper;
	
	@Test
	public void testInsertUser() {
		User user = new User();
		user.setUsername("test1");
		user.setPassword("test1");
		user.setPicture("testAddress");
		userMapper.insert(user);
	}

}
